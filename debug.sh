#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
}

mkdir -p logs

echo "Running gunicorn on 80 port..."
rm -rf logs/gunicorn.log
touch logs/gunicorn.log
gunicorn --preload --log-file=logs/gunicorn.log -D -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker" frontend.frontend:app -b 0.0.0.0:80 --workers=2

echo "Tailing logs/python.log"
echo "======================================="
rm -rf logs/python.log
touch logs/python.log
tail -f logs/python.log
echo "======================================="
echo "Stoppnig gunicorn..."
pkill -9 gunicorn
pkill -9 frontend 
pkill -9 backend
pkill -9 python
exit 0
