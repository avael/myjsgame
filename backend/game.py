import gevent
from gevent import Greenlet
import uuid
from objects.environment import Barrel
from objects.characters import Player
import logging
import inspect
import sys
import types
import json


log = logging.getLogger()


class Client(Greenlet):

    def __init__(self, socket):
        Greenlet.__init__(self)
        self.id = uuid.uuid1()
        self.socket = socket
        log.info("Client %s created" % self.id)

    def command(self, command, data):
        command = {"command": command}
        command.update(data)
        self.socket.send(json.dumps(command))

    def messageHandler(self, message):
        pass

    def _run(self):
        while True:
            message = self.socket.receive()
            self.messageHandler(message)


class Game(Greenlet):

    def __init__(self, app):
        Greenlet.__init__(self)
        self.app = app
        self.clients = []

    def onJoin(self, client):
        for module in [k for k in sys.modules.keys() if k.startswith("objects.")]:
            for name, obj in inspect.getmembers(sys.modules[module], inspect.isclass):
                if obj is not types.NoneType:
                    client.command('reciveSpriteUrls', {"urls": obj.Sprites})

    def joinClient(self, clientWebSocket):
        client = Client(clientWebSocket)
        client.start()
        self.clients.append(client)
        self.app.logger.info("Now serving %i clients" % len(self.clients))
        self.onJoin(client)
        return client.id

    def _run(self):
        self.app.logger.info("Game greenlet started (%s)" % id(self))
        while True:
            # self.app.logger.info("Game loop")
            gevent.sleep(1)
        self.app.logger.info("Game greenlet stopping (%s)" % id(self))
