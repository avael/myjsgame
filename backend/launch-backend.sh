#!/bin/sh

mkdir -p logs
echo "Running backend on $1 port..."
python backend/backend.py $1 >> logs/python.log &
